# https://hub.docker.com/_/microsoft-dotnet
FROM mcr.microsoft.com/dotnet/sdk:3.1 AS build
LABEL stage=intermediate

ARG GITLAB_PACKAGE_REGISTRY_USERNAME
ENV GITLAB_PACKAGE_REGISTRY_USERNAME=${GITLAB_PACKAGE_REGISTRY_USERNAME}

ARG GITLAB_PACKAGE_REGISTRY_PASSWORD
ENV GITLAB_PACKAGE_REGISTRY_PASSWORD=${GITLAB_PACKAGE_REGISTRY_PASSWORD}

WORKDIR /source

# copy csproj and restore as distinct layers
COPY *.sln .
COPY src/DemoProject.WebApi/*.csproj ./src/DemoProject.WebApi/
COPY test/DemoProject.WebApi.Tests/*.csproj ./test/DemoProject.WebApi.Tests/

# execute nuget dll restore
RUN dotnet restore

# can't not include folder obj/**, use the .dockerignore or it will cause error
COPY src/DemoProject.WebApi/. ./src/DemoProject.WebApi/

# -c: Configuration
# -o: output file
# --no-restore: no use dotnet restore
RUN dotnet publish -c release -o /app --no-restore

# final stage/image
FROM mcr.microsoft.com/dotnet/aspnet:3.1-alpine
ARG APP_VERSION
ENV APP_VERSION=${APP_VERSION:-1.0.0}
ARG PORT=80
ENV ASPNETCORE_URLS=http://*:${PORT}

LABEL appVersion=${APP_VERSION}

WORKDIR /app
COPY --from=build /app ./
 
EXPOSE ${PORT}
ENTRYPOINT ["dotnet", "DemoProject.WebApi.dll"]



